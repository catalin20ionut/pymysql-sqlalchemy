import pymysql

db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

with db.cursor() as c:
    c.execute("""
        CREATE TABLE IF NOT EXISTS bikesharing (
            tstamp timestamp,
            cnt integer,
            temperature decimal(5, 2),
            temperature_feels decimal(5, 2),
            humidity decimal(4, 1),
            wind_speed decimal(5,2),
            weather_code integer,
            is_holiday boolean,
            is_weekend boolean,
            season integer
        );
    """)

db.close()
