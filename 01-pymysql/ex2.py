import pymysql
import csv
from datetime import datetime

''' Funcționează doar dacă london-bikes es află în folderul 01-pymysql. --------- select count(*) from bikesharing; '''
db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

with db.cursor() as c:
    with open("london-bikes.csv") as input_file:
        dict_reader = csv.DictReader(input_file)
        i = 1
        for row in dict_reader:
            # print(row)
            # {'timestamp': '2015-01-04 00:00:00', 'cnt': '182', 'temperature': '3.0', 'temperature_feels': '2.0',
            # 'humidity': '93.0', 'wind_speed': '6.0', 'weather_code': '3.0', 'is_holiday': '0.0', 'is_weekend': '1.0',
            # 'season': '3.0'}
            timestamp = datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S')
            # strptime primește data ca string și al doilea parametru este formatul în care se află data
            # https://docs.python.org/3/library/datetime.html?highlight=datetime#strftime-strptime-behavior
            cnt = int(row['cnt'])
            temperature = float(row['temperature'])
            temperature_feels = float(row['temperature_feels'])
            humidity = float(row['humidity'])
            wind_speed = float(row['wind_speed'])
            weather_code = int(float(row['weather_code']))
            is_holiday = True if row['is_holiday'] == '1.0' else False
            is_weekend = True if row['is_weekend'] == '1.0' else False
            season = int(float(row['season']))

            c.execute("INSERT INTO bikesharing VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);",
                      (timestamp, cnt, temperature, temperature_feels, humidity, wind_speed, weather_code, is_holiday,
                       is_weekend, season))

            i += 1

            if i % 100 == 0:                                             # în multiplu de 100 (se termina in 2 zerouri)
                db.commit()

        db.commit()                          # facem commit și pentru cele care rămân în afară (sunt mai puține de 100)

db.close()
