import pymysql
import csv
from datetime import datetime

db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

with db.cursor() as c:
    with open("london-bikes.csv") as input_file:
        reader = csv.reader(input_file)
        i = 1
        for row in reader:
            if i == 1:                       # sărim peste capul de tabel
                i += 1
                continue
            # print(row)  # ['2015-01-04 00:00:00', '182', '3.0', '2.0', '93.0', '6.0', '3.0', '0.0', '1.0', '3.0']
            timestamp = datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S')
            cnt = int(row[1])
            temperature = float(row[2])
            temperature_feels = float(row[3])
            humidity = float(row[4])
            wind_speed = float(row[5])
            weather_code = int(float(row[6]))
            is_holiday = True if row[7] == '1.0' else False
            is_weekend = True if row[8] == '1.0' else False
            season = int(float(row[9]))

            c.execute("INSERT INTO bikesharing VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);",
                      (timestamp, cnt, temperature, temperature_feels, humidity, wind_speed, weather_code, is_holiday,
                       is_weekend, season))

            i += 1

            if i % 100 == 0:
                db.commit()

        db.commit()

db.close()
