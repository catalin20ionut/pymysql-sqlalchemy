import pymysql

db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

with db.cursor() as c:
    # fetch total sum of new shares by season
    c.execute("SELECT season, SUM(cnt) from bikesharing GROUP BY season;")
    print(c.fetchall())

    # fetch total sum of new shares during thunderstorms
    c.execute("SELECT SUM(cnt) FROM bikesharing WHERE weather_code = 10;")
    print(f"Nr of shares: {c.fetchone()[0]}")

    # fetch the date and hour with the most new shares
    c.execute("SELECT tstamp, cnt FROM bikesharing ORDER BY cnt DESC LIMIT 1;")
    # sau cu where: SELECT tstamp, cnt FROM bikesharing WHERE cnt = (SELECT MAX(cnt) FROM bikesharing);
    print(c.fetchone())

db.close()

''' Comenzi mysql:
USE `default`;
SELECT season, SUM(cnt) from bikesharing GROUP BY season;
SELECT SUM(cnt) FROM bikesharing WHERE weather_code = 10;
SELECT tstamp, cnt FROM bikesharing ORDER BY cnt DESC LIMIT 1;
SELECT tstamp, cnt FROM bikesharing WHERE cnt = (SELECT MAX(cnt) FROM bikesharing);
'''
