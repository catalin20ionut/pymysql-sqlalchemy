import pymysql

db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

with db.cursor() as c:
    # Add 10 to cnt column for all 2015-01-09 entries
    c.execute("UPDATE bikesharing SET cnt = cnt + 10 WHERE tstamp LIKE '2015-01-09%';")
    db.commit()  # pentru că modificăm baza de date cu un UPDATE; ...
    # ... se folosește de fiecare dată când se face o modificare în baza de date

db.close()
''' Comandă mysql >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
select * from bikesharing WHERE tstamp LIKE '2015-01-09%'; 
'''