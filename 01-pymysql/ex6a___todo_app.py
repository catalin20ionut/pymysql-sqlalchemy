import pymysql
''' todo_app'''


def create_structure():
    db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="default")

    with db.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS todo_app;")
        c.execute("""
            CREATE TABLE IF NOT EXISTS todo_app.tasks (
                id int not null auto_increment primary key,
                task text not null,
                done boolean
            );
        """)  # am pus todo_app.tasks în loc să facem USE todo_app și după create table

    db.close()


create_structure()
