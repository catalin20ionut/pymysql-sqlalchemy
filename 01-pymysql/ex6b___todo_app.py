import pymysql


def create_structure():
    db = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="")

    with db.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS todo_app;")
        c.execute("""
            CREATE TABLE IF NOT EXISTS todo_app.tasks (
                id int not null auto_increment primary key,
                task text not null,
                done boolean
            );
        """)  # am pus todo_app.tasks în loc să facem USE todo_app și după create table

    db.close()


create_structure()


def show_menu():
    print("1. Show task list")
    print("2. Mark task as done")
    print("3. Add new task")
    print("4. Delete one task")
    print("0. Exit application")


connection = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="todo_app")

# with connection.cursor() as c:
#     c.execute("UPDATE tasks SET task = 'terminare exercitiu' WHERE id = 1")
#     connection.commit()

while True:
    show_menu()
    optiune = int(input("Opțiunea este: "))

    if optiune >= 5 or optiune < 0:
        print("Invalid option")
    else:
        if optiune == 0:
            print("The app is closing.")
            break
        elif optiune == 3:
            with connection.cursor() as c:
                task_name = input("Task name: ")
                c.execute("INSERT INTO tasks (task, done) VALUES (%s, %s);", (task_name, False))
                connection.commit()
        elif optiune == 1:
            with connection.cursor() as c:
                c.execute("SELECT * FROM tasks WHERE done = 0 ORDER BY id DESC;")
                results = c.fetchall()
                print("The list of tasks:")
                for result in results:
                    print(f"Id is {result[0]} and name is {result[1]}")
                print("--------------")
        elif optiune == 2:
            with connection.cursor() as c:
                task_id = int(input("Task id: "))
                c.execute("UPDATE tasks SET done = 1 WHERE id = %s;", (task_id,))
                connection.commit()
        elif optiune == 4:
            with connection.cursor() as c:
                task_id = int(input("Task id: "))
                c.execute("DELETE FROM tasks WHERE id = %s;", (task_id,))
                connection.commit()

connection.close()
