import pymysql
# create the database thecinematicbase


def create_structure():
    conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="")

    with conn.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS thecinematicbase;")
        c.execute("""CREATE TABLE IF NOT EXISTS thecinematicbase.directors (
            director_id INT PRIMARY KEY AUTO_INCREMENT,
            name TEXT NOT NULL,
            surname TEXT NOT NULL,
            rating INT NOT NULL
        );""")
        c.execute("""
            CREATE TABLE IF NOT EXISTS thecinematicbase.movies (
                movie_id INT PRIMARY KEY AUTO_INCREMENT,
                title TEXT NOT NULL,
                year INT NOT NULL,
                category TEXT NOT NULL,
                director_id INT NOT NULL,
                CONSTRAINT fk_director_id FOREIGN KEY(director_id) REFERENCES thecinematicbase.directors(director_id),
                rating INT NOT NULL
            );
        """)

    conn.close()

create_structure()
