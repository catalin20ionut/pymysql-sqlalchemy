import pymysql


# 1. Write an SQL query to display all the movies from 2002.
# 2. Write an SQL query to display all the director names for crime movies.
# 3. Write an SQL query to display all the movies with rating more than 6 (or equal) and director rating ..............
# ..............................................................................................more than 7 (or equal).


def display_movies():
    conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="thecinematicbase")

    with conn.cursor() as c:
        c.execute("""SELECT title, year FROM movies WHERE year=2007;""")
        print(c.fetchall())

        c.execute("""SELECT movies.title, movies.category, movies.director_id, directors.name, directors.surname,
        directors.director_id FROM directors INNER JOIN movies ON  movies.director_id = directors.director_id WHERE 
        category='Crime';""")
        print(c.fetchall())

        c.execute(""" SELECT movies.title, movies.category, movies.director_id,movies.rating, directors.name, 
        directors.surname,directors.director_id, directors.rating FROM directors INNER JOIN movies ON  
        movies.director_id = directors.director_id WHERE movies.rating >= 6 and directors.rating >= 7; """)
        print(c.fetchall())

display_movies()
