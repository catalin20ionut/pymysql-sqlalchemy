"""
    1. O să avem niște utilizatori care trebuie sa aibă neapărat un nume, email, parolă.
    2. O să avem produse pe care utilizatorii le pot cumpăra dacă sunt disponibile.
       Un produs are denumire, cantitate, preț.

    Înainte să fie logat utilizatorul:
    1. Log-in (cerem de la utilizator email și parolă)
    2. Register (cerem de la utilizator nume, email și parolă și îi facem un cont)
    0. Exit application

    După login:
    1. Afișează lista de produse disponibile (afișăm produsele care au cantitatea > 0)
    2. Cumpără produs (îi cerem un id de produs și o cantitate, iar dacă exista suficientă cantitate îl lăsăm să cumpere
    (scade cantitatea de pe produs și se adaugă în istoric cumpărătura)
    3. Afișează istoric cumpărături (îi afișăm ce produs a cumpărat, când l-a cumpărat (data și ora) și cât a plătit în
    total (cantitate * preț))
    4. Log out - se întoarce la primul meniu
    0. Exit application

    Date:
Nume = alex
Email = alex@alex.com
Parola = 1234

    În mysql în tabelul produs scriem comenzile:
INSERT INTO produs (denumire, cantitate, pret) VALUES ("faina", "8", "2.32");
INSERT INTO produs (denumire, cantitate, pret) VALUES ("apa", "15", "2.10");
INSERT INTO produs (denumire, cantitate, pret) VALUES ("orez", "45", "5.45");
INSERT INTO produs (denumire, cantitate, pret) VALUES ("zahar", "81", "3.45");
INSERT INTO produs (denumire, cantitate, pret) VALUES ("ulei", "130", "6.79");
"""

import pymysql
import datetime


def create_structure():
    conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="")

    with conn.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS shop;")
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.utilizator (
                id INT PRIMARY KEY AUTO_INCREMENT,
                nume TEXT NOT NULL,
                email TEXT NOT NULL,
                parola TEXT NOT NULL
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.produs (
                id INT PRIMARY KEY AUTO_INCREMENT,
                denumire TEXT NOT NULL,
                cantitate INT NOT NULL,
                pret DECIMAL(8, 2) NOT NULL 
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.istoric (
                id INT PRIMARY KEY AUTO_INCREMENT,
                user_id INT NOT NULL,
                produs_id INT NOT NULL,
                data TIMESTAMP NOT NULL,
                cantitatea_cumparata INT NOT NULL,
                CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES shop.utilizator(id),
                CONSTRAINT fk_produs_id FOREIGN KEY(produs_id) REFERENCES shop.produs(id)
            );
        """)

    conn.close()


create_structure()


def show_menu_1():
    print("1. Login")
    print("2. Register")
    print("0. Exit application")


def show_menu_2():
    print("1. Show product list")
    print("2. Buy product")
    print("3. Show history")
    print("4. Logout")
    print("0. Exit application")


conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="shop")

user_logat = None

while True:
    if user_logat is None:
        show_menu_1()
        optiune = int(input("Opțiunea este: "))

        if optiune == 0:
            break
        elif optiune == 1:
            email = input("Email = ")
            parola = input("Parola = ")

            with conn.cursor() as c:
                c.execute("SELECT id FROM utilizator WHERE email = %s AND parola = %s;", (email, parola))
                result = c.fetchone()
                # fetch one ne returnează None dacă nu s-au găsit date (dacă nu există email-ul cu parola respectiva)
                if result:
                    user_logat = result[0]
                else:
                    print("Username/Parola nu exista.")
        elif optiune == 2:
            nume = input("Nume = ")
            email = input("Email = ")
            parola = input("Parola = ")

            with conn.cursor() as c:
                c.execute("INSERT INTO utilizator (nume, email, parola) VALUES (%s, %s, %s);", (nume, email, parola))
                conn.commit()
    else:
        show_menu_2()
        optiune = int(input("Opțiunea este: "))

        if optiune == 0:
            break
        elif optiune == 1:            # afișează lista de produse disponibile (afișăm produsele care au cantitatea > 0)
            with conn.cursor() as c:
                c.execute("SELECT * FROM produs WHERE cantitate > 0")
                produse = c.fetchall()
                print("Lista de produse:")
                for produs in produse:
                    print(f"Produsul {produs[1]} cu id-ul {produs[0]} are cantitatea {produs[2]} si prețul {produs[3]}"
                          f" lei")
                print("--------------------")
        elif optiune == 2:
            # Cumpără produs (îi cerem un id de produs și o cantitate și dacă există suficientă cantitate îl lăsăm ...
            # ... să cumpere (scade cantitatea de pe produs și se adaugă în istoric cumpărătura)
            id_produs = int(input("Id produs = "))
            cantitatea = int(input("Cantitatea = "))

            with conn.cursor() as c:
                c.execute("SELECT * FROM produs WHERE id = %s AND cantitate >= %s;", (id_produs, cantitatea))
                result = c.fetchone()
                if result:
                    # exista produsul si cantitatea dorita
                    c.execute("UPDATE produs SET cantitate = cantitate - %s WHERE id = %s;", (cantitatea, id_produs))
                    c.execute("INSERT INTO istoric (user_id, produs_id, data, cantitatea_cumparata) "
                              "VALUES (%s, %s, %s, %s);",
                              (user_logat, id_produs, datetime.datetime.now(), cantitatea))
                    conn.commit()
                else:
                    print("Nu exista produsul sau cantitate suficienta")
        elif optiune == 3:                                                               # afișează istoric cumpărături
            with conn.cursor() as c:
                c.execute("SELECT p.denumire, p.pret * i.cantitatea_cumparata, i.data FROM istoric i "
                          "INNER JOIN produs p ON i.produs_id = p.id WHERE i.user_id = %s;", (user_logat,))
                results = c.fetchall()
                print("Istoric cumpărături: ")
                for result in results:
                    print(f"Ați cumpărat {result[0]} in valoare de {result[1]} lei la data de {result[2]}")
                print("--------------------")
        elif optiune == 4:
            user_logat = None

conn.close()
