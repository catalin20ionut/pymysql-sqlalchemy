import pymysql
''' Ordine fișiere
example-01
example-02
example-03
ex1
ex2 (cu dict_reader)
ex2_reader_simple
example-04
ex3
ex4 etc.
'''

db = pymysql.connect(host="127.0.0.1", user="root", password="Mysqlsdacurs2021", database="")
''' 
host      -> unde este hostat server-ul de baze de date
127.0.0.1 -> localhost -> server-ul de baze de date rulează local (la fiecare pe PC)
user      -> username-ul pe care l-ați configurat când ați instalat MySQL (de obicei e root)
password  -> parola cu care va conectați și din Workbench
database  -> care reprezintă baza de date la care să ne conectăm (baza de date care este pe server)
          -> daca nu vrem să ne conectăm la o baza de date ci doar la server, atunci punem un string gol
          -> nu vrem să ne conectam la o bază de date în momentul în care vrem să creem sau să ștergem una
          -> când vrem să executăm comenzi care au legătură cu server-ul (cum ar fi să luăm versiunea de server)
Pentru a executa o comandă o să avem nevoie de fiecare data de un cursor. '''


with db.cursor() as c:
    c.execute("SELECT VERSION()")
    version = c.fetchone()                          # fetchone returnează valoarea de pe primul rând din rezultat
    print(version)
    print(f"Database version: {version[0]}")    # version[0] -> valoarea primei (si singura) coloane din rezultat
'''
db.cursor()    -> e doar o funcție care ne returnează un obiect de tipul cursor pe care îl putem folosi pentru
                  execute, fetchone etc
execute        -> trimitem comenzi de SQL către server. 
                  În acest caz, comanda trimisă o să ne returneze versiunea de MySQL.
fetchone       -> returnează valoarea de pe primul rând din rezultat
print(version) -> rezultatul este întors sub formă de tuplu,iar tuplul conține toate valorile coloanelor pe un
                  rând (în cazul nostru e un rând și doar o coloană)
version[0]     -> valoarea primei (și singurei) coloane din rezultat
'''

db.close()      # închide conexiunea cu serverul de baze de date
