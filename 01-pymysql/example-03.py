import pymysql

db = pymysql.connect(host="127.0.0.1", user="root", password="Mysqlsdacurs2021", database="default")
# ne conectăm la baza de date pe care am creat-o în exemplul anterior

with db.cursor() as c:                                                    # ne luăm cursor ca să putem executa comenzi
    c.execute("CREATE TABLE IF NOT EXISTS `example3` (a integer, b varchar(255));")
    # executăm o comandă prin care să creem un tabel pentru baza noastră de date
    a = int(input("Please provide an integer value for a: "))                       # cerem un număr de la utilizator
    b = input("Please provide value for b: ")[:254]
    # cerem o valoare pentru b (un string) - maxim 254 de caractere (0-253)
    c.execute("INSERT INTO `example3` (a, b) VALUES (%s, %s);", (a, b))
    # dăm parametrii unei comenzi de SQL folosindu-ne de %s
    # valorile pentru fiecare %s trebuie să fie într-un tuplu, al doilea parametru de la funcția execute
    # câte %s avem, atâtea valori trebuie să avem in tuplu
    # primul %s o sa fie înlocuit cu prima valoare din tuplu, al doilea %s o să fie înlocuit cu a doua valoare etc.
    # ne protejează de SQL Injection (când utilizatorul ne dă un text care este parte dintr-o comandă SQL prin care ...
    # ... de obicei intenționează să șteargă date)
    db.commit()
    # commit se dă de fiecare dată când vrem să executăm modificări pe baza de date: ..................................
    # ..................................................................... INSERT, UPDATE, DELETE (COMMIT TRANSACTION)

db.close()                                                                                       # închidem conexiunea
