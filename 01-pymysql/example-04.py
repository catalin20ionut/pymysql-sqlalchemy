import pymysql
import datetime

db = pymysql.connect(host="127.0.0.1", user="root", password="Mysqlsdacurs2021", database="default")
with db.cursor() as c:
    c.execute(
        "SELECT tstamp,cnt FROM bikesharing WHERE tstamp BETWEEN %s AND %s",
        (datetime.datetime(2016, 1, 1, 0), datetime.datetime(2016, 1, 1, 5)),
    )             # luăm timestamp-ul și numărul de share-uri între 1 Ian 2016 ora 00:00:00 și 1 Ian 2016 ora 05:00:00
    print(c.description)
    print(f"Column names: {[d[0] for d in c.description]}")
    print(c.fetchone())                                                                                # first row
    print(c.fetchall())                                                                                # remaining rows
    print(f"Got {c.rowcount} rows")
    print(c.fetchone())                                             # None -> pentru că nu mai avem rânduri în rezultat

db.close()
