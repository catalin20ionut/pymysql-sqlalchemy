from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Task

''' Înainte de a rula programul pentru prima dată în mysql scrie: create database todo_app2; '''
CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="todo_app2")
)

Base.metadata.create_all(eng)  # creează tabelul in DB
Session = sessionmaker(bind=eng)
s = Session()


def show_menu():
    print("1. Show task list")
    print("2. Mark task as done")
    print("3. Add new task")
    print("4. Delete one task")
    print("0. Exit application")


while True:
    show_menu()
    optiune = int(input("Opțiunea este: "))

    if optiune >= 5 or optiune < 0:
        print("Invalid option")
    else:
        if optiune == 0:
            print("The app is closing.")
            break
        elif optiune == 3:
            task_name = input("Task name: ")
            # INSERT INTO tasks (task, done) VALUES (%s, %s); (task_name, False)
            s.add(Task(task=task_name, done=False))
            s.commit()
        elif optiune == 1:
            # SELECT * FROM tasks WHERE done != 1;
            tasks = s.query(Task).filter(Task.done != True).all()
            print("The list of tasks:")
            for task in tasks:
                print(f"Id is {task.id} and name is {task.task}")
            print("--------------")
        elif optiune == 2:
            task_id = int(input("Task id: "))
            task = s.query(Task).filter(Task.id == task_id).first()
            task.done = True
            # UPDATE tasks SET done = 1 WHERE id = <task_id>;
            s.commit()
        elif optiune == 4:
            task_id = int(input("Task id: "))
            task = s.query(Task).filter(Task.id == task_id).first()
            s.delete(task)
            # DETELE FROM tasks WHERE id = <task_id>;
            s.commit()
