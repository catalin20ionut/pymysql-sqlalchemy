from sqlalchemy import create_engine
from models import Base

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"
# la începutul CONNECTION_STRING-ului avem tipul de baza de date + librăria cu care se va conecta la baza de date

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default")
)                                      # create_engine face o conexiune la baza de date

Base.metadata.create_all(eng)
# creează tabelele in DB dacă nu există deja
# Base e clasa de baza din care moștenesc toate clasele care sunt tabele in baza de date
