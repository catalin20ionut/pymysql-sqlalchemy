from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Student

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default")
)

Session = sessionmaker(bind=eng)                                          # creem o clasă care ne da accesul la sesiune
s = Session()                                                                                # ne instanțiem o sesiune
# s = sessionmaker(bind=eng)()  -> alternativa la cele 2 linii de mai sus

s.add_all(
    [
        Student(first_name="Mike", last_name="Wazowski"),
        Student(first_name="Netti", last_name="Nashe"),
        Student(first_name="Jessamine", last_name="Addison"),
        Student(first_name="Brena", last_name="Bugdale"),
        Student(first_name="Theobald", last_name="Oram"),
    ]
)
s.commit()
