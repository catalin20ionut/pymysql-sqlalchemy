from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Student


CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default"),  # echo=True
)

Session = sessionmaker(bind=eng)
s = Session()

print("1a. All the students >>> ")
# SELECT * FROM students;
rows = s.query(Student).all()
for row in rows:
    print(row)

print("1b. All the students")
# SELECT first_name, last_name FROM students;
# când nu vrem toate atributele/coloanele din tabel atunci se returnează rezultatul sub forma de lista de tupluri
rows = s.query(Student.first_name, Student.last_name).all()
for row in rows:
    print(row)

print("-------")
# SELECT COUNT(*) FROM students;
total = s.query(Student).count()
print(f"Total: {total} students")

print("--------")
# SELECT * FROM students WHERE id >= 2 AND first_name LIKE 'Bre%';
query_result = s.query(Student).filter(Student.id >= 2, Student.first_name.like("Bre%"))
# daca lăsăm comanda așa, ea nu este executată până nu o forțează cineva (un for sau punem noi .all() după ea)
for row in query_result:                                               # forțează executarea query_result-ului
    print("Varianta 1 >>> Found students:", row)

# SELECT * FROM students WHERE id >= 2 AND first_name LIKE 'Bre%';
query_result = s.query(Student).filter(Student.id >= 2, Student.first_name.like("Bre%")).all()
for row in query_result:
    print("Varianta 2 >>> Found students:", row)

query_result = s.query(Student.first_name, Student.last_name).filter(
    Student.id >= 2, Student.first_name.like("Bre%")).all()
for row2 in query_result:
    print("Varianta 3 >>> Found students:", row2)
