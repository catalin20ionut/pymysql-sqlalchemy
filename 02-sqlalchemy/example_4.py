from sqlalchemy.exc import IntegrityError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Student, Locker

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default")
)

Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

# corespondent lui INSERT
try:
    s.add_all(
        [
            Locker(number=1, student=4),
            Locker(number=2, student=1),
            Locker(number=3, student=5),
            Locker(number=4, student=2),
            Locker(number=5, student=3),
        ]
    )
    s.commit()
except IntegrityError:
    s.rollback()  # ROLLBACK TRANSACTION
    print("Lockers already created!")

# SELECT * FROM students s INNER JOIN lockers l ON s.id = l.student WHERE l.number = 4;
rows = s.query(Student, Locker).join(Locker).filter(Locker.number == 4)
for row in rows:  # rows e o lista de tupluri
    student, locker = row
# row = un tuplu dintre un student si un locker
# a, b = (1, 2) -> a = 1, b = 2
# student = row[0]
# locker = row[1]
    print(f"Student with locker #{locker.number} : {student}")
