from sqlalchemy.exc import IntegrityError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Student, Address, Locker

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default")
)

Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

# Insert
try:
    s.add_all(
        [
            Address(student=1, street_name="Teilor", number=10, city="București"),
            Address(student=2, street_name="Brâncoveanu", number=2, city="București"),
            Address(student=3, street_name="Mihai Viteazu", number=85, city="Cluj-Napoca"),
            Address(student=4, street_name="Teilor", number=97, city="București"),
            Address(student=5, street_name="Dorobanți", number=150, city="Cluj-Napoca"),
        ]
    )
    s.commit()
except IntegrityError:
    s.rollback()
    print("Addresses already added for existing students.")

# SELECT * FROM students s JOIN address a ON s.id = a.student;
results = s.query(Student, Address).join(Address).all()
for result in results:
    student, address = result
    print(f"Studentul {student.first_name} {student.last_name} locuiește la {address}")

# SELECT s.first_name, s.last_name, a.number, a.street_name, a.city FROM students s JOIN address a ON s.id = a.student;
results = s.query(Student.first_name, Student.last_name, Address.number, Address.street_name,
                  Address.city).join(Address).all()
for result in results:
    print(result)

# SELECT * FROM students s JOIN address a ON s.id = a.student JOIN lockers l ON s.id = l.student;
results = s.query(Student, Address, Locker).join(Address, Locker).all()
for result in results:
    print(result)
