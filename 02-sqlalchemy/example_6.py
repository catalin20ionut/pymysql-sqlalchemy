from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Grade
from datetime import datetime

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="default"),  # echo=True
)
Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

s.add_all(
    [
        Grade(student=1, grade=10, date_created=datetime.now()),
        Grade(student=1, grade=3, date_created=datetime.now()),
        Grade(student=1, grade=8, date_created=datetime.now()),
        Grade(student=2, grade=8, date_created=datetime.now()),
        Grade(student=2, grade=9, date_created=datetime.now()),
        Grade(student=3, grade=6, date_created=datetime.now()),
        Grade(student=4, grade=10, date_created=datetime.now()),
        Grade(student=4, grade=10, date_created=datetime.now()),
        Grade(student=4, grade=9, date_created=datetime.now()),
        Grade(student=5, grade=9, date_created=datetime.now()),
        Grade(student=5, grade=10, date_created=datetime.now()),
    ]
)
s.commit()

for student_id in range(1, 6):
    grades = s.query(Grade.grade).filter(Grade.student == student_id).all()
    # # result = (nota,) tuplu cu un singur element (nota studentului)
    # print(f"1. Studentul cu id-ul {student_id} a primit notele {grades}")  # se primesc notele suf formă de tuplu
    print(f"Studentul cu id-ul {student_id} a primit notele "
          f"{', '.join(list(map(lambda result: str(result[0]), grades)))}")
