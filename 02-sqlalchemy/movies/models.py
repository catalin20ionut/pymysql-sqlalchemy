from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, ForeignKey


Base = declarative_base()


class Directors(Base):
    __tablename__ = "directors"

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text, nullable=False)
    surname = Column(Text, nullable=False)
    rating = Column(Integer, nullable=False)


class Movies(Base):
    __tablename__ = "movies"

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(Text, nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(Text, nullable=False)
    director_id = Column(Integer, ForeignKey(Directors.director_id), nullable=False)
    rating = Column(Integer, nullable=False)              # nullable=False (nullable = is able to be null) -> NOT NULL
