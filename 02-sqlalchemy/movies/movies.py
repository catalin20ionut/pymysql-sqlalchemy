from sqlalchemy import create_engine, or_, func, text, column
from sqlalchemy.orm import sessionmaker
from models import Base, Movies, Directors

''' Înainte de a rula programul pentru prima dată în mysql scrie: create database cinematic; '''

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="cinematic")
)

Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

movies = [
    {
        "title": "The Shawshank Redemption",
        "year": 1994,
        "category": "Drama",
        "director_id": 1,
        "rating": 8,
    },
    {
        "title": "The Green Mile",
        "year": 1999,
        "category": "Drama",
        "director_id": 1,
        "rating": 6,
    },
    {
        "title": "The Godfather",
        "year": 1972,
        "category": "Crime",
        "director_id": 2,
        "rating": 7,
    },
    {
        "title": "The Godfather III",
        "year": 1990,
        "category": "Crime",
        "director_id": 2,
        "rating": 6,
    },
    {
        "title": "Pulp Fiction",
        "year": 1994,
        "category": "Crime",
        "director_id": 3,
        "rating": 9,
    },
    {
        "title": "Inglourious Basterds",
        "year": 2009,
        "category": "War",
        "director_id": 3,
        "rating": 8,
    },
    {
        "title": "The Dark Knight",
        "year": 2008,
        "category": "Action",
        "director_id": 4,
        "rating": 9,
    },
    {
        "title": "Interstellar",
        "year": 2014,
        "category": "Sci-fi",
        "director_id": 4,
        "rating": 8,
    },
    {
        "title": "The Prestige",
        "year": 2006,
        "category": "Drama",
        "director_id": 4,
        "rating": 10,
    },
    {
        "title": "Fight Club",
        "year": 1999,
        "category": "Drama",
        "director_id": 5,
        "rating": 7,
    },
    {
        "title": "Zodiac",
        "year": 2007,
        "category": "Crime",
        "director_id": 5,
        "rating": 5,
    },
    {
        "title": "Seven",
        "year": 1995,
        "category": "Drama",
        "director_id": 5,
        "rating": 8,
    },
    {
        "title": "Alien 3",
        "year": 1992,
        "category": "Horror",
        "director_id": 5,
        "rating": 5,
    },
]


def add_data(directors, movies):
    for director in directors:
        s.add(Directors(name=director['name'], surname=director['surname'], rating=director['rating']))

    for movie in movies:
        s.add(Movies(title=movie['title'], year=movie['year'], category=movie['category'],
                     director_id=movie['director_id'], rating=movie['rating']))

    s.commit()


def drama_movies():
    """
    Print all movies for the Drama category.
    Use select () and query ().
    """
    movies = s.query(Movies).filter(Movies.category == 'Drama')
    print("1. Drame: ")
    for movie in movies:
        print(f"Filmul {movie.title} apărut in anul {movie.year}")
    print('------------')


def crime_movies():
    """
    Print the titles of movies from the Crime category that were made after 1994.
    Use select () and query ().
    """
    movies = s.query(Movies).filter(Movies.category == 'Crime', Movies.year > 1994)
    print("2. Crime după 1994: ")
    for movie in movies:
        print(f"Filmul {movie.title} apărut in anul {movie.year}")
    print('------------')


def rating_movies():
    """
    Print the categories of all movies and their ratings,
    for the films that were produced between 2000 and 2010
    and had ratings greater than 7, also, sort them by their ratings.
    Use select () and query ().
    """
    movies = s.query(Movies).filter(Movies.rating > 7, Movies.year.between(2000, 2010)).order_by(Movies.rating)
    # pentru DESC -> Movies.rating.desc()
    print("3. Filme făcute intre 2000 si 2010 cu rating mai mare ca 7: ")
    for movie in movies:
        print(f"Filmul {movie.title} apărut in anul {movie.year}, gen {movie.category} cu rating {movie.rating}")
    print('------------')


def rating_directors():
    """
    Print the names of all directors whose ratings is greater than or equal to 6
    and whose first name starts with 'D' or ends with 'n'.
    Use select () and query ().
    """
    directors = s.query(Directors).filter(Directors.rating >= 6,
                                          or_(Directors.name.like('D%'), Directors.name.like('%n')))
    print('4. Regizori care au ranting mai mare decât 5 și prenumele începe cu D sau se termină cu n:')
    for director in directors:
        print(f"{director.name} {director.surname}")
    print('------------')


def director_movies():
    """
    List the names and surnames of all directors who made their films between 2011 and 2014 and whose films were rated
    lower than 9. Use select () and query ().
    """

    """SELECT directors.* FROM directors d INNER JOIN movies m ON d.director_id = m.director_id 
    WHERE m.year BETWEEN 2011 AND 2014 AND m.rating < 9;"""
    directors = s.query(Directors).join(Movies).filter(Movies.year.between(2011, 2014), Movies.rating < 9)
    print("5. Regizori cu filme intre 2011 si 2014 care au filme cu ranting mai mic decât 9:")
    for director in directors:
        print(f"Regizorul {director.name} {director.surname}")
    print('-----------')


def avg_movies_director():
    """
    List the total number of films created by each director,
    their full name, and the average rating for each director
    based on their film ratings. Use select () and query ().
    """
    # func (from sqlalchemy import func): conține toate funcțiile de SQL
    # cum ar fi cele de agregare (COUNT, AVG, SUM etc)

    '''
    SELECT d.name, d.surname, COUNT(*), AVG(m.rating)
    FROM directors d
    JOIN movies m ON d.director_id = m.director_id
    GROUP BY m.director_id;
    '''
    results = s.query(Directors.name, Directors.surname, func.count(Movies.title),
                      func.round(func.avg(Movies.rating), 2)).join(Movies).group_by(Movies.director_id)
    # pt round la AVG:
    # https://www.w3schools.com/sql/func_mysql_round.asp
    # ROUND(AVG(m.rating), 2) = func.round(fund.avg(Movies.rating), 2)
    print("6. Regizor, nr. de filme, rating mediu")
    for result in results:     # result = (<director_name>, <director_surname>, count(movie_title), avg(movie_rating))
        print(f"Regizorul {result[0]} {result[1]} are {result[2]} filme cu un rating mediu de {result[3]}")
    print('-----------')


def avg_movies_director_sql1():
    # putem sa executam si query-ul de SQL prin sqlalchemy
    sql_text = text("""
        SELECT d.name, d.surname, COUNT(*), ROUND(AVG(m.rating), 2)
        FROM directors d
        JOIN movies m ON d.director_id = m.director_id
        WHERE m.year BETWEEN :start AND :stop
        GROUP BY m.director_id;        
    """)
    print("7a", s.execute(sql_text, {'start': 1980, 'stop': 2021}).all())


def avg_movies_director_sql2():
    # putem sa executam si query-ul de SQL prin sqlalchemy
    sql_text = text("""
        SELECT d.name, d.surname, COUNT(*) AS nr_movies, ROUND(AVG(m.rating), 2) AS avg_rating
        FROM directors d
        JOIN movies m ON d.director_id = m.director_id
        WHERE m.year BETWEEN :start AND :stop
        GROUP BY m.director_id;        
    """)
    print("7b.", s.query(column('name'), column('surname'), column('nr_movies'),
                         column('avg_rating')).from_statement(sql_text).params(start=1980, stop=2021).all())


directors = [
    {"name": "Frank", "surname": "Darabont", "rating": 7},
    {"name": "Francis Ford", "surname": "Coppola", "rating": 8},
    {"name": "Quentin", "surname": "Tarantino", "rating": 10},
    {"name": "Christopher", "surname": "Nolan", "rating": 9},
    {"name": "David", "surname": "Fincher", "rating": 7},
]


# add_data(directors, movies)  # DE EXECUTAT O SINGURĂ DATĂ
drama_movies()
crime_movies()
rating_movies()
rating_directors()
director_movies()
avg_movies_director()
avg_movies_director_sql1()
avg_movies_director_sql2()
