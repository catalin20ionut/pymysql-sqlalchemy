import pymysql
from datetime import datetime
from dateutil.parser import parse  # DON'T FORGET: pip install python-dateutil

''' CREATE DATABASE trainers1;'''


def create_structure():
    conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="")

    with conn.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS trainers1;")
        c.execute(
            """
            CREATE TABLE IF NOT EXISTS trainers1.trainer (
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                name TEXT NOT NULL,
                email TEXT NOT NULL,
                location TEXT NOT NULL,
                contract TEXT NOT NULL,
                password TEXT NOT NULL,
                UNIQUE KEY idx_email (email(255))
            );
        """
        )
        c.execute(
            """
            CREATE TABLE IF NOT EXISTS trainers1.session (
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                number_of_hours INT NOT NULL,
                start_date TIMESTAMP NOT NULL,
                stop_date TIMESTAMP NOT NULL,
                order_no TEXT NOT NULL,
                base_price DECIMAL(8, 2) NOT NULL,
                group_id TEXT NOT NULL,
                is_paid BOOLEAN NOT NULL DEFAULT 0,
                trainer_id INT NOT NULL,
                CONSTRAINT fk_trainer_id FOREIGN KEY(trainer_id) REFERENCES trainers1.trainer(id)
            );
        """
        )
        # c.execute("ALTER TABLE trainers1.trainer ADD UNIQUE (email(255));")

    conn.close()


def show_menu1():
    print("1. Login")
    print("2. Register")
    print("0. Exit")


def show_menu2():
    print("1. Add training session")
    print("2. Get upcomming training sessions")
    print("3. Get unpaid training sessions")
    print("4. Generate bill")
    print("5. Get total paid sum in a year")
    print("6. Logout")
    print("0. Exit")


create_structure()

conn = pymysql.connect(host="localhost", user="root", password="Mysqlsdacurs2021", database="trainers1")

user_logat = None

while True:
    if user_logat is None:
        show_menu1()
        optiune = int(input("Optiune = "))

        if optiune == 0:
            break
        elif optiune == 1:
            email = input("Email = ")
            parola = input("Parola = ")

            with conn.cursor() as c:
                c.execute(
                    "SELECT id FROM trainer WHERE email = %s AND password = %s;",
                    (email, parola),
                )
                result = c.fetchone()
                if result:
                    user_logat = result[0]  # result = (id_user,)
                else:
                    print("Combinația email/parola este greșită")
        elif optiune == 2:
            name = input("Nume = ")
            email = input("Email = ")
            location = input("Location = ")
            contract = input("Contract No = ")
            parola = input("Parola = ")

            with conn.cursor() as dog:
                dog.execute(
                    "INSERT INTO trainer (name, email, location, contract, password) VALUES (%s, %s, %s, %s, %s);",
                    (name, email, location, contract, parola),
                )

                conn.commit()
    else:
        show_menu2()
        optiune = int(input("Optiune = "))

        if optiune == 0:
            break
        elif optiune == 6:
            user_logat = None
        elif optiune == 1:
            number_of_hours = int(input("Număr de ore = "))
            start_date = parse(input("Dată început = "))
            stop_date = parse(input("Dată sfârșit = "))
            order_no = input("Order No = ")
            base_price = float(input("Pret pe ora = "))
            group_id = input("Group id = ")

            with conn.cursor() as c:
                c.execute(
                    """
                   INSERT INTO session(number_of_hours, start_date, stop_date, order_no, base_price, group_id,
                    trainer_id) VALUES (%s, %s, %s, %s, %s, %s, %s);
                """,
                    (
                        number_of_hours,
                        start_date,
                        stop_date,
                        order_no,
                        base_price,
                        group_id,
                        user_logat,
                    ),
                )

                conn.commit()
        elif optiune == 2:
            with conn.cursor() as c:
                c.execute("SELECT * from session WHERE start_date > %s AND trainer_id = %s;",
                          (datetime.now(), user_logat))
                results = c.fetchall()
                for result in results:
                    print(result)
        elif optiune == 3:
            with conn.cursor() as c:
                c.execute("SELECT * FROM session WHERE is_paid = 0 AND trainer_id = %s;", (user_logat,))
                results = c.fetchall()
                for result in results:
                    print(result)
        elif optiune == 4:
            group_id = input("Group id = ")
            start_date = parse(input("Start date = "))
            stop_date = parse(input("Stop date = "))

            with conn.cursor() as c:
                c.execute("SELECT * FROM session WHERE group_id = %s AND start_date "
                          "BETWEEN %s AND %s AND stop_date BETWEEN %s AND %s AND trainer_id = %s;",
                          (group_id, start_date, stop_date, start_date, stop_date, user_logat))
                results = c.fetchall()
                for result in results:  # result = (id, ......)
                    c.execute("UPDATE session SET is_paid = 1 WHERE id = %s;", (result[0], ))

                conn.commit()
        elif optiune == 5:
            year = int(input("An = "))

            with conn.cursor() as c:
                c.execute("SELECT SUM(base_price * number_of_hours) FROM session "
                          "WHERE is_paid = 1 AND trainer_id = %s AND start_date BETWEEN %s AND %s AND stop_date "
                          "BETWEEN %s AND %s;",
                          (user_logat, datetime(year, 1, 1, 0, 0, 0), datetime(year, 12, 31, 23, 59, 59),
                           datetime(year, 1, 1, 0, 0, 0), datetime(year, 12, 31, 23, 59, 59)))
                result = c.fetchone()
                print(f"Ai primit {result[0]} Lei")
conn.close()
