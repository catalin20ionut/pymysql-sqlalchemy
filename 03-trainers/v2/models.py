# don't forget CREATE DATABASE trainers2;
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, ForeignKey, Text, Boolean, DECIMAL, TIMESTAMP, String


Base = declarative_base()


class Trainer(Base):
    __tablename__ = "trainer"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text, nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    location = Column(Text, nullable=False)
    contract = Column(Text, nullable=False)
    password = Column(Text, nullable=False)


class Session(Base):
    __tablename__ = "session"

    id = Column(Integer, primary_key=True, autoincrement=True)
    numberOfHours = Column(Integer, nullable=False)
    startDate = Column(TIMESTAMP, nullable=False)
    stopDate = Column(TIMESTAMP, nullable=False)
    orderNo = Column(Text, nullable=False)
    basePrice = Column(DECIMAL(8, 2), nullable=False)
    groupId = Column(Text, nullable=False)
    isPaid = Column(Boolean, nullable=False, default=False)
    trainer_id = Column(Integer, ForeignKey(Trainer.id), nullable=False)
