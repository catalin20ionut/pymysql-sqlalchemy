# don't forget CREATE DATABASE trainers2;
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from dateutil.parser import parse
from datetime import datetime
from models import Base, Trainer, Session

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(user="root", password="Mysqlsdacurs2021", host="127.0.0.1", db="trainers2")
)
Base.metadata.create_all(eng)
DatabaseSession = sessionmaker(bind=eng)
s = DatabaseSession()


def show_menu1():
    print("1. Login")
    print("2. Register")
    print("0. Exit")


def show_menu2():
    print("1. Add training session")
    print("2. Get upcomming training sessions")
    print("3. Get unpaid training sessions")
    print("4. Generate bill")
    print("5. Get total paid sum in a year")
    print("6. Logout")
    print("0. Exit")


user_logat = None

while True:
    if user_logat is None:
        show_menu1()
        optiune = int(input("Optiune = "))

        if optiune == 0:
            break
        elif optiune == 1:
            email = input("Email = ")
            parola = input("Parola = ")

            result = s.query(Trainer.id).filter(Trainer.email == email, Trainer.password == parola).first()
            # .first() -> fetchone(), .all() -> fetchall()
            if result:
                user_logat = result[0]
            else:
                print("Combinația email/parola este greșită")
        elif optiune == 2:
            name = input("Nume = ")
            email = input("Email = ")
            location = input("Location = ")
            contract = input("Contract No = ")
            parola = input("Parola = ")

            s.add(Trainer(name=name, email=email, location=location, contract=contract, password=parola))
            s.commit()
    else:
        show_menu2()
        optiune = int(input("Optiune = "))

        if optiune == 0:
            break
        elif optiune == 6:
            user_logat = None
        elif optiune == 1:
            number_of_hours = int(input("Număr de ore = "))
            start_date = parse(input("Data de început = "))
            stop_date = parse(input("Data de sfârșit = "))
            order_no = input("Order No = ")
            base_price = float(input("Preț pe oră = "))
            group_id = input("Group id = ")

            s.add(Session(numberOfHours=number_of_hours, startDate=start_date, stopDate=stop_date,
                          orderNo=order_no, basePrice=base_price, groupId=group_id, trainer_id=user_logat))
            s.commit()
        elif optiune == 2:
            results = s.query(Session).filter(Session.startDate > datetime.now(),
                                              Session.trainer_id == user_logat).all()
            for result in results:
                print(f"{result.id}, {result.numberOfHours}, {result.groupId}")
        elif optiune == 3:
            results = s.query(Session).filter(Session.isPaid == False, Session.trainer_id == user_logat).all()
            for result in results:
                print(f"{result.id}, {result.numberOfHours}, {result.groupId}")
        elif optiune == 4:
            group_id = input("Group id = ")
            start_date = parse(input("Start date = "))
            stop_date = parse(input("Stop date = "))

            results = s.query(Session).filter(
                Session.groupId == group_id,
                Session.startDate.between(start_date, stop_date),
                Session.stopDate.between(start_date, stop_date),
                Session.trainer_id == user_logat
            ).all()

            for result in results:
                result.isPaid = True

            s.commit()
        elif optiune == 5:
            year = int(input("An = "))

            result = s.query(func.sum(Session.basePrice * Session.numberOfHours)).filter(
                Session.isPaid == True,
                Session.trainer_id == user_logat,
                Session.startDate.between(datetime(year, 1, 1, 0, 0, 0), datetime(year, 12, 31, 23, 59, 59)),
                Session.stopDate.between(datetime(year, 1, 1, 0, 0, 0), datetime(year, 12, 31, 23, 59, 59))
            ).first()
            print(f"Ai primit {result[0]} Lei")
